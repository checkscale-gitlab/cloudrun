#!/usr/bin/env bash

set -eo pipefail


prep=$( echo $MOUNT_GCS | tr '+' ' ' )

for point in $prep; do
    
    set -- `echo $point | tr '=' ' '`
    path=$2

    set -- `echo $1 | tr ':' ' '`
    echo $2
    bucket=$1; folder=$2

    mkdir -p $path
    
    
    if [[ $folder = "/" ]]
        then
        # mount bucket

            if [[ $DEBUG_GCS = true ]]
                then
                    gcsfuse --debug_gcs --debug_fuse $bucket $path || exit 1;
                    echo "The GCS $bucket was mounted in $path with debug flag"
                else
                    gcsfuse $bucket $path || exit 1;
                    echo "The GCS $bucket was mounted in $path"
            fi

        else
        # mount folder
    
            if [[ $DEBUG_GCS = true ]]
                then
                    gcsfuse --debug_gcs --debug_fuse --only-dir $folder $bucket $path || exit 1;
                    echo "The $folder from GCS $bucket was mounted in $path with debug flag"
                else
                    gcsfuse --only-dir $folder $bucket $path || exit 1;
                    echo "The $folder from GCS $bucket was mounted in $path"
            fi
    fi
    
done

./run.sh &

# Exit immediately when one of the background processes terminate.
wait -n
